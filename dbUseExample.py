# -*- coding: utf-8 -*-
"""
Created on Wed Jul  1 11:05:35 2020

@author: Stefan
"""

import pandas as pd, numpy, sqlite3, PIL, requests, io, cv2
con = sqlite3.connect("ygo.db")
#def imgFromUrl(url):
#    response = requests.get(url)
#    return PIL.Image.open(io.BytesIO(response.content))
# Monsters
df_m = pd.read_sql_query("SELECT * FROM mcards", con)

#Spells
df_s = pd.read_sql_query("SELECT id, name, cats, des FROM stcards WHERE spell=1", con)

#Traps
df_t = pd.read_sql_query("SELECT id, name, cats, des FROM stcards WHERE spell=0", con)

#Pendulum Monsters with Pendulum Description and Scale
df_pen = pd.read_sql_query("""SELECT * FROM mcards 
                           JOIN pend USING(id)""", con)
#Link Monsters with Arrows
df_link = pd.read_sql_query("""SELECT * FROM mcards
                            JOIN link USING(id)""", con)

#All Lvl 4 Synchro Monsters
df_lvl4_syn = pd.read_sql_query("""SELECT id, name, des 
                                FROM mcards 
                                WHERE cats LIKE '%Synchro%'
                                AND deg = 4""", con)

# All Pendulum Xyz Monsters
df_pen_xyz = pd.read_sql_query("""SELECT *
                                FROM mcards c
                                JOIN pend p 
                                USING(id)
                                WHERE c.cats LIKE '%XYZ%'""", con)

#get Apollousa
apo = pd.read_sql_query("""SELECT * FROM mcards
                            JOIN link USING(id)
                            JOIN cardimages USING(id)
                            WHERE name LIKE 'Apollousa%'""", con)
#imgFromUrl(apo['url'][0])