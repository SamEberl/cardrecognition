# -*- coding: utf-8 -*-
"""
Created on Tue Jun 30 17:37:31 2020

@author: Stefan
"""

import sqlite3, requests, time, ast

class api_requests:
    def __init__(self):
        self.lastRTime = time.time()
        #self.url = r"https://db.ygoprodeck.com/api/v7/cardinfo.php"
    def request(self, par = ""):
        req = r"https://db.ygoprodeck.com/api/v7/cardinfo.php"
        if(par != ""):
            req += "?"+par
        t = time.time()
        if(t - self.lastRTime < 0.5):
            time.sleep(0.5 - (t - self.lastRTime))
            self.lastRTime = 0.5 + self.lastRTime
        else:
            self.lastRTime = t
        response = requests.get(req)
        return response.content.decode('UTF-8')
    def getCardList(self):
        return ast.literal_eval(self.request())['data']
        
    
api_req = api_requests()
cl = api_req.getCardList()

conn = sqlite3.connect("ygo.db")
c = conn.cursor()

if True:
    c.execute("""
          CREATE TABLE IF NOT EXISTS mcards
          (id INT NOT NULL PRIMARY KEY, 
          name VARCHAR(128) NOT NULL UNIQUE, 
          cats VARCHAR(128),
          ATK SMALLINT, 
          DEF SMALLINT, 
          deg TINYINT,
          des TEXT, 
          type VARCHAR(32), 
          att VARCHAR(16))
          WITHOUT ROWID;
          """)
    c.execute("""
          CREATE TABLE IF NOT EXISTS stcards
          (id INT NOT NULL PRIMARY KEY,
          name VARCHAR(128) NOT NULL UNIQUE,
          spell BOOL, 
          cats VARCHAR(128), 
          des TEXT)
          WITHOUT ROWID;
          """)
    c.execute("""
          CREATE TABLE IF NOT EXISTS pend
          (id INT NOT NULL PRIMARY KEY,
          scale TINYINT, 
          pen_des TEXT)
          WITHOUT ROWID;
          """)
    c.execute("""
        CREATE TABLE IF NOT EXISTS link
        (id INT NOT NULL PRIMARY KEY,
        tl BOOL,
        t BOOl,
        tr BOOl,
        l BOOl,
        r BOOl,
        bl BOOL,
        b BOOL,
        br BOOL)
        WITHOUT ROWID;""")
    c.execute("""
              CREATE TABLE IF NOT EXISTS cardimages
              (id INT NOT NULL PRIMARY KEY,
              url TEXT,
              url_small TEXT)
              WITHOUT ROWID;""")
lm = {'Top-Left' : 0, 'Top' : 1, 'Top-Right' : 2, 'Left' : 3, 'Right' : 4, 
      'Bottom-Left' : 5, 'Bottom' : 6, 'Bottom-Right' : 7}
if True:
    tables = c.execute("SELECT name FROM sqlite_master WHERE type = 'table'").fetchall()
    print("Tables:", tables)
    for t in tables:
        #TODO: BAD! SQL Injection possible
        print(t[0])
        entries = c.execute("PRAGMA table_info('"+t[0]+"')").fetchall()
        for e in entries:
            print(e)
        print()
if True:
    for i in range(len(cl)):
        car = cl[i]
        ide = car['id']
        if car['type'] == "Spell Card":
            val = (ide, car['name'], 1, car['race'], car['desc'])
            c.execute("INSERT INTO stcards VALUES (?,?,?,?,?)", val)
        if car['type'] == "Trap Card":
            val = (ide, car['name'], 0, car['race'], car['desc'])
            c.execute("INSERT INTO stcards VALUES (?,?,?,?,?)", val)
        if "Monster" in car['type']:
            cats = ','.join(car['type'].split(" ")[:-1])
            deg = None #degree: lvl, rank or link rating
            defe = None #defense
            des = None
            pen_des = None
            if 'Pendulum' in car['type']:
                desc = car['desc']
                if "[ Pendulum Effect ]" in desc:
                    splitString = "[ Flavor Text ]" if "[ Flavor Text ]" in desc else "[ Monster Effect ]"
                    pen_des, des = car['desc'].split(splitString)
                    pen_des = pen_des.strip().split("[ Pendulum Effect ]")[1].rstrip("-").strip()
                else:
                    des = desc
                    pen_des = ""
                scale = car['scale']
                c.execute("INSERT INTO pend VALUES (?,?,?)", (ide, scale, pen_des))
            else:
                des = car['desc']
            des = des.strip()
            if 'linkmarkers' in car.keys():
                deg = car['linkval']
                linkVal = [ide,0,0,0,0,0,0,0,0]
                for mar in car['linkmarkers']:
                    linkVal[lm[mar]+1] = 1
                c.execute("INSERT INTO link VALUES (?,?,?,?,?,?,?,?,?)", linkVal)
            if 'level' in car.keys():
                deg = car['level']
            if 'def' in car.keys():
                defe = car['def']
            val = (ide, car['name'], cats, car['atk'], defe, deg, des,
                   car['race'], car['attribute'])
            c.execute("INSERT INTO mcards VALUES (?,?,?,?,?,?,?,?,?)", val)
        imageUrl = car['card_images'][0]['image_url']
        imageUrl_small = car['card_images'][0]['image_url_small']
        c.execute("INSERT INTO cardimages VALUES (?,?,?)", (ide, imageUrl, imageUrl_small))
conn.commit()
conn.close()
#c.execute("select name from sqlite_master where type = 'table'")
#print(c.fetchall())