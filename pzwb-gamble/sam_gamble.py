# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 12:18:16 2020

@author: Stefan
"""

import numpy as np, scipy.stats, matplotlib.pyplot as plt


#We are position 'pos' after 'step' steps.
#If we end the game our payoff is 'payoff(steps)'
#Considering that our payoff becomes 0 once 'pos' exceeds 21
#Should we keep going?
#If 'Yes', then return True
#If 'No', return False


#pos and setp are in the non-neg. integers N (including 0)
#payoff is a function N -> N

# def proper_round(num,dec=0):
#     num = str(num)[:str(num).index('.')+dec+2]
#     if num[-1]>='5':
#         return float(num[:-2-(not dec)]+str(int(num[-2-(not dec)])+1))
#     return float(num[:-1])

def stop(pos, step, payoff):
    # print(f"{pos}, {step}, {step*step}")
    # reward = 0
    # for i in range(1,15):
    #     chance_current_round = 1 / (2**i)
    #     reward_current_round = 2 * step + i + 1
    #     reward += chance_current_round * reward_current_round
    # reward = int(reward)

    reward = 2 * step + 1
    chance_reward = 1 - (1/(2**(21-pos)))
    loss = step * step
    chance_loss = 1/(2**(21-pos))

    gain = (reward * chance_reward) - (loss * chance_loss)

    # print(f"reward({reward})*chance({chance_reward}) = {reward*chance_reward}")
    # print(f"loss({loss})*chance({chance_loss}) = {loss*chance_loss}")
    # print(f"gain: {gain}\n")
    if(gain < 0):
        return True
    else:
        return False

def play(M = 21, stop = stop, payoff = lambda n : n*n):
    k=0
    x = np.zeros(500)
    while(x[k] < 21 and not stop(x[k], k, payoff)):
        k+=1
        z = np.random.geometric(p=0.5) - 1
        x[k] += x[k-1] + z
    if(x[k] >= 21):
        return (k, 0, x[0:k+1])
    else:
        return (k, payoff(k), x[0:k+1])

k, y, x = play()    
print("Stop at", int(x[k]), "after", k, "steps, with payoff", y)
plt.xticks(np.arange(1, k+1, 1))
plt.yticks(np.arange(1, 30, 1))
plt.plot(np.arange(1, k+2, 1), x, 'ro')
plt.show()