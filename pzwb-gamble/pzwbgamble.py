# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 12:18:16 2020

@author: Stefan
"""

import numpy as np, scipy.stats, matplotlib.pyplot as plt


#We are position 'pos' after 'step' steps.
#If we end the game our payoff is 'payoff(steps)'
#Considering that our payoff becomes 0 once 'pos' exceeds 21
#Should we keep going?
#If 'Yes', then return True
#If 'No', return False


#pos and setp are in the non-neg. integers N (including 0)
#payoff is a function N -> N
def stop(pos, step, payoff):
    #write your own stopping function here
    if(0 == 1):
        return True
    else:
        return False

def play(M = 21, stop = stop, payoff = lambda n : n*n):
    k=0
    x = np.zeros(500)
    while(x[k] < 21 and not stop(x[k], k, payoff)):
        k+=1
        z = np.random.geometric(p=0.5) - 1
        x[k] += x[k-1] + z
    if(x[k] >= 21):
        return (k, 0, x[0:k+1])
    else:
        return (k, payoff(k), x[0:k+1])
k, y, x = play()    
print("Stop at", int(x[k]), "after", k, "steps, with payoff", y)
plt.xticks(np.arange(1, k+1, 1))
plt.yticks(np.arange(1, 30, 1))
plt.plot(np.arange(1, k+2, 1), x, 'ro')
plt.show()