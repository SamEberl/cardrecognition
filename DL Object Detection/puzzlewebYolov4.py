# -*- coding: utf-8 -*-
"""
Created on Wed Aug 12 23:35:33 2020

@author: Stefan Perko
"""

import torch

class DropBlock2D(torch.nn.Module):
    r"""Randomly zeroes spatial blocks of the input tensor.
    As described in the paper
    `DropBlock: A regularization method for convolutional networks`_ ,
    dropping whole blocks of feature map allows to remove semantic
    information as compared to regular dropout.
    Args:
        keep_prob (float, optional): probability of an element to be kept.
        Authors recommend to linearly decrease this value from 1 to desired
        value.
        block_size (int, optional): size of the block. Block size in paper
        usually equals last feature map dimensions.
    Shape:
        - Input: :math:`(N, C, H, W)`
        - Output: :math:`(N, C, H, W)` (same shape as input)
    .. _DropBlock: A regularization method for convolutional networks:
       https://arxiv.org/abs/1810.12890
    """
    def __init__(self, keep_prob = 0.9, block_size = 7):
        super().__init__()


#Convolution -> BatchNormalization -> Activation -> DropBlock
class ConvBlock(torch.nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride, 
                 activation, coord = False):
        super().__init__()
        padding = (kernel_size - 1) // 2
        
        #inplace = True means the argument is changed, instead of a copy of it
        a = {'relu' : torch.nn.ReLU(inplace = True), 'leaky' : torch.nn.LeakyReLU(.1, inplace = True)}
        self.module = torch.nn.Sequential(torch.nn.Conv2d(in_channels, out_channels, kernel_size,
                                            stride, padding),
                                          a[activation],
                                          DropBlock2D())
#in_channels -> 2*nf = 64
#this is slightly different from the other downsampleblocks
#where domains and codomains are concerned, 
#unfortunately in a way that is not easily reconciled without
#becoming confusing
#that's why we have an extra class for this layer
class DownSampleFirst(torch.nn.Module):
    def __init__(self, in_channels = 3, activation = "leaky"):
        nf = 32 #nr of filters after first ConvBlock
        self.c1 = ConvBlock(in_channels, nf, 3, 1, activation)
        self.c2 = ConvBlock(nf, 2*nf, 3, 2, activation)
        self.c3 = ConvBlock(2*nf, 2*nf, 1, 1, activation)
        self.c4 = ConvBlock(2*nf, nf, 1, 1, activation)
        self.c5 = ConvBlock(nf, 2*nf, 3, 1, activation)
        self.c6 = ConvBlock(2*nf, 2*nf, 1, 1, activation)
        self.csp = ConvBlock(2*nf, 2*nf, 1, 1, activation)
        self.c7 = ConvBlock(4*nf, 2*nf, 1, 1, activation)
    def forward(self, x):
        x1 = self.c1(x)
        x2 = self.c2(x1)
        x3 = self.c3(x2)
        x4 = self.c4(x3)
        x5 = self.c5(x4)
        x6 = x3 + x5
        x7 = self.c6(x6)
        x8 = self.csp(x2)
        x9 = torch.cat([x7, x8], dim = 1)
        x10 = self.c7(x9)
        return x10
    
    
class ResBlock(torch.nn.Module):
    def __init__(self, ch, n_res = 1, activation = "leaky"):
        super().__init__()
        self.res_mods = torch.nn.ModuleList()
        for i in range(n_res):
            self.res_mods.append(ConvBlock(ch, ch, 1, 1, activation))
            self.res_mods.append(ConvBlock(ch, ch, 3, 1, activation))
        self.dropblock = DropBlock2D()
    def forward(self, x):
        h = x
        for mod in self.res_mods:
            h = mod(h)
        x = x + h
        x = self.dropblock(x)
        return x
        

#in_channels -> 2*in_channels
#the out_channels argument seems superflous here
#the architecture forces it to be 2*in_channels
class DownSampleBlock(torch.nn.Module):
    def __init__(self, in_channels, n_res = 1, activation = "leaky"):
        nf = in_channels  
        self.c2 = ConvBlock(nf, 2*nf, 3, 2, activation)
        self.c3 = ConvBlock(2*nf, 2*nf, 1, 1, activation)
        self.c4 = ConvBlock(2*nf, nf, 1, 1, activation)
        self.c5 = ConvBlock(nf, 2*nf, 3, 1, activation)
        self.c6 = ConvBlock(2*nf, 2*nf, 1, 1, activation)
        self.csp = ConvBlock(2*nf, 2*nf, 1, 1, activation)
        self.c7 = ConvBlock(4*nf, 2*nf, 1, 1, activation)
    def forward(self, x):
        x2 = self.c2(x)
        x3 = self.c3(x2)
        x4 = self.c4(x3)
        x5 = self.c5(x4)
        x6 = x3 + x5
        x7 = self.c6(x6)
        x8 = self.csp(x2)
        x9 = torch.cat([x7, x8], dim = 1)
        x10 = self.c7(x9)
        return x10        
class Backbone(torch.nn.Module):
    def __init__(self, in_channels):
        super().__init__()
        self.c1 = ConvBlock(in_channels, 32 , 3 , 1, "leaky")
    def forward(self, x):
        return self.c1(x)

#comes with Dropblock
class Neck(torch.nn.Module):
    def __init__(self, spp_kernels = (5,9,13), PAN_layers = [512, 256]):
        super().__init__()
    def forward(self, x):
        pass
    
class Head(torch.nn.Module):
    def __init__(self, out_channels):
        super().__init__()
    def forward(self, x):
        pass
    
# one dense prediction layer
class YOLOLayer(torch.nn.Module):
    def __init__(self, anchors, n_classes, img_dim):
        super().__init__()
    def forward(self, x):
        pass
        
class YOLOv4(torch.nn.Module):
    def __init__(self, n_classes = 80, in_channels = 3, img_dim = 608, anchors = None):
        super().__init__()
        if anchors is None:
        #TODO: Relevance of these coordinates?
            anchors = [[[10, 13], [16, 30], [33, 23]], #(w,h), (w,h), (w,h)
                       [[30, 61], [62, 45], [59, 119]],
                       [[116, 90], [156, 198], [373, 326]]]
        #for B = 3 boxes we predict
        #4 box coordinates tx, ty, tw, th
        #1 objectness score po
        #classes scores for every class
        out_channels = (4 + 1 + n_classes) * 3
        
        self.img_dim = img_dim
        
        self.backbone = Backbone(in_channels)
        self.neck = Neck()
        self.head = Head(out_channels)
        
        self.yolo1 = YOLOLayer(anchors[0], n_classes, img_dim)
        self.yolo2 = YOLOLayer(anchors[1], n_classes, img_dim)       
        self.yolo3 = YOLOLayer(anchors[2], n_classes, img_dim)
        
    def forward(self, x):
        b = self.backbone(x)
        return b
        
if __name__ == "__main__":    
    yolo = YOLOv4(2)
        