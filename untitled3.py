# -*- coding: utf-8 -*-
"""
Created on Tue Jun 30 16:36:14 2020

@author: Stefan
"""

import cv2, numpy as np, pytesseract as tessa, sqlite3, pandas as pd
con = sqlite3.connect("ygo.db")

tessa.pytesseract.tesseract_cmd = r"C:\Program Files\Tesseract-OCR\tesseract.exe"

img = cv2.imread("yugioh-trickstar-candina-ultra-near-mint-1st-edition-mp18-en037-name.jpg", 0)
#img = img * 1
#img[img > 255] = 255
#img = img.astype(np.int)
#img = cv2.GaussianBlur(img, (3,3), 0)
img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
nameOCR = tessa.image_to_string(img, lang='eng', config='--psm 7')
df_m = pd.read_sql_query("SELECT * FROM mcards", con)


cv2.imshow('image',img)
cv2.waitKey(0)
cv2.destroyAllWindows()