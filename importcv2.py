import cv2, numpy as np

img_paths = [r"yugioh-trickstar-candina-ultra-near-mint-1st-edition-mp18-en037.jpg",
             r"skytry.jpg",
             r"s-l300.jpg",
             r"trap.jpg",
             r"bagooska.jpg",
             r"8c0dd49a78c3984ec75a3b1bf64cf10d.jpg"]
img = cv2.imread(img_paths[0])

#Determining contour
#img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#_, img_thresh = cv2.threshold(img_gray, 127, 255, 0)
lower = np.array([35, 62, 71])
#upper = np.array([137, 130, 138])
upper = np.array([178, 164, 155])
mask = cv2.inRange(img, lower, upper)
output = cv2.bitwise_and(img, img, mask=mask)
output = cv2.cvtColor(output, cv2.COLOR_BGR2GRAY)
contours, hierarchy = cv2.findContours(output, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
# For each contour, find the convex hull and draw it
# on the original image.
contourFusion = np.zeros([0,1,2], dtype = 'int32')
for i in range(len(contours)):
    if(contours[i].shape[0] > 0):
        contourFusion = np.vstack([contourFusion, contours[i]])

#for i in range(len(contours)):
hull = cv2.convexHull(contourFusion)
#rect = cv2.minAreaRect(hull)
#box = cv2.boxPoints(rect)
#box = np.int0(box)
epsilon = 0.1*cv2.arcLength(hull,True)
approx = cv2.approxPolyDP(hull,epsilon,True)
cv2.drawContours(img,[approx],0,(0,0,255),2)
#cv2.drawContours(img, [hull], -1, (255, 0, 0), 2)

#cv2.imshow('image', mask)
cv2.imshow('image', img)
cv2.waitKey(0)
cv2.destroyAllWindows()