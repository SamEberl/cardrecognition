# CardRecognition

This project has the goal of developing an App to recognize playing cards and display their current market value.<br/>
\[Nutzen beschreiben\]<br/>
The project is for done for educational purposes by the PuzzleWeb Group in Jena.

## Approach
The information contained about a card or multiple cards is extracted with the use of machine vision techniques.<br/>
This information gets transformed into a representation by using machine learning techniques.<br/>
The representation can be compared to a database containing the cards in question.<br/>
The assumption is that this way even new cards could be classified correctly.

## Team
Maxi: Medizin<br/>
Stefan: Mathematik<br/>
Alexia: Psychologie<br/>
Sam: Maschinenbau<br/>

## Time commitment
Stefan:2h/week<br/>
Maxi: 2h/week<br/>
Alexia:<br/>
Sam: 2h/week<br/>

## Projektstrukturplan (PSP) test draft
![Projektstrukturplan_PSP](images/PSP.png)

## Timeline test draft
![Timeline](images/Timeline.PNG)