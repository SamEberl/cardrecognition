# -*- coding: utf-8 -*-
"""
Created on Sun Oct 18 18:33:02 2020

@author: Stefan
"""

import numpy as np, scipy.stats, matplotlib.pyplot as plt
from numba import jit

from functools import partial

def neverStop(pos, step, payoff):
    #write your own stopping function here
    if(0 == 1):
        return True
    else:
        return False
    
def stop80_20(pos, step, payoff):
    return np.random.binomial(1, 0.2, 1) == 1
p = 0.5
M = 21

def stopSnell(pos, step, payoff):
    def expYx(x, n, count = 100):
        out = np.ones(count)
        for m in range(count):
            out[m] = scipy.stats.nbinom.cdf(x-1, m, p)*payoff(m+n)
        return out
    N = np.argmax(expYx(M - pos, step))
    if(N == 0):
        return True
    else:
        return False 
    
def stopSnell1(pos, step, payoff):
    def expYx(x, n, count = 1):
        out = np.ones(count)
        for m in range(count):
            out[m] = scipy.stats.nbinom.cdf(x-1, m, p)*payoff(m+n)
        return out
    N = np.argmax(expYx(M - pos, step))
    if(N == 0):
        return True
    else:
        return False
    
def stopSnellOff(pos, step, payoff):
    def expYx(x, n, count = 100):
        out = np.ones(count)
        for m in range(count):
            out[m] = scipy.stats.nbinom.cdf(x-1, m+1, p)*payoff(m+1+n)
        return out
    N = np.argmax(expYx(M - pos, step))
    if(N == 0):
        return True
    else:
        return False 
    
def stopSnell1Off(pos, step, payoff):
    def expYx(x, n, count = 1):
        out = np.ones(count)
        for m in range(count):
            out[m] = scipy.stats.nbinom.cdf(x-1, m+1, p)*payoff(m+1+n)
        return out
    N = np.argmax(expYx(M - pos, step))
    if(N == 0):
        return True
    else:
        return False

    
def stopMaxi(pos, step, payoff):
    n=21-pos
    terma=(((((0.5)**(n+1))-1)/(0.5-1))-1)*(step+1)**2
    termb=-payoff(step)*(2-((((0.5)**(n+1))-1)/(0.5-1)))
   
    if(payoff(step)>terma+termb):

    #if(0==1):
        return True
    else:
        return False
    
def stopSam2(pos, step, payoff):

    M = 21
    
    fields = M + 1 - int(pos)
    
    arr = np.zeros((fields, fields), float)
    for m in range(fields):
        for n in range(fields):
            if n+m < fields:
                arr[m, n+m] = 1/2**(n+1)
    arr[:, -1] = arr[:, -1]*2

    rounds = fields * 2

    probs = np.empty((rounds, fields), float)
    probs[0] = arr[0]
    for i in range(1, rounds):
        probs[i] = np.matmul(probs[i-1], arr)

    chance_reward = np.sum(probs[:, 0:fields-1], axis=1)

    reward = np.empty((rounds, 1))
    for i in range(rounds):
        reward[i] = chance_reward[i] * payoff(step+i+1)
    max_reward = np.max(reward)

    #print(f'max_reward: {max_reward:.3f} --- payoff: {payoff(step)} --- index: {np.argmax(reward)+1}')
    if(max_reward < payoff(step)):
        return True
    else:
        return False
    
def stopSam(pos, step, payoff):
    # print(f"{pos}, {step}, {step*step}")
    # reward = 0
    # for i in range(1,15):
    #     chance_current_round = 1 / (2**i)
    #     reward_current_round = 2 * step + i + 1
    #     reward += chance_current_round * reward_current_round
    # reward = int(reward)

    reward = 2 * step + 1
    chance_reward = 1 - (1/(2**(21-pos)))
    loss = (step) * (step)
    chance_loss = 1/(2**(21-pos))

    gain = (reward * chance_reward) - (loss * chance_loss)

    # print(f"reward({reward})*chance({chance_reward}) = {reward*chance_reward}")
    # print(f"loss({loss})*chance({chance_loss}) = {loss*chance_loss}")
    # print(f"gain: {gain}\n")
    if(gain < 0):
        return True
    else:
        return False    
    
def play(M = 21, stop = neverStop, payoff = lambda n : n*n):
    k=0
    x = np.zeros(500)
    while(x[k] < M and not stop(x[k], k, payoff)):
        k+=1
        z = np.random.geometric(p=p) - 1
        x[k] += x[k-1] + z
    if(x[k] >= M):
        return (k, 0, x[0:k+1])
    else:
        return (k, payoff(k), x[0:k+1])
    
def playMulti(M = 21, stops = [], payoff = lambda n : n*n):
    k=0
    x = np.zeros(500)
    n_play = len(stops)
    k_stop = np.zeros(n_play)
    earnings = np.zeros(n_play)
    while(x[k] < M):
        for i in range(n_play):
            if stops[i](x[k], k, payoff):
                k_stop[i] = k
                earnings[i] = payoff(k) if x[k] < M else 0
        k+=1
        z = np.random.geometric(p=p) - 1
        x[k] += x[k-1] + z
    for i in range(n_play):
        if k_stop[i] == 0:
            k_stop[i] = k
    return (k, x[0:k+1], k_stop, earnings)


stops = [stopSnell, stopSnell1, stopSnellOff, stopSnell1Off, stopMaxi, stopSam]
print("Snell100, Snell1, Snell100Off, Snell1Off, Maxi, Sam")
k_r, x_r, k_stop_r, earnings_r = [], [], [], []
total_earnings = np.zeros(len(stops))
for i in range(10000):
    k, x, k_stop, earnings = playMulti(M, stops = stops, 
                                       payoff = lambda n : n*n)
    k_r.append(k)
    x_r.append(x)
    k_stop_r.append(k_stop)
    earnings_r.append(earnings)
    total_earnings += earnings
    #print("Earnings: ", earnings)
    #print("Stops after", k_stop, "steps.")
    #pos_stop = np.zeros(len(stops))
    #print("Stopped at", pos_stop)
    #for j in range(len(stops)):
    #    pos_stop[j] = x[k_stop[j]]
    #print("We hit ", int(x[k]), "after", k, "steps.")
    #plt.xticks(np.arange(1, k+1, 1))
    #plt.yticks(np.arange(1, 30, 1))
    #plt.plot(np.arange(1, k+2, 1), x, 'ro')
    #plt.show()
    if i % 50 == 0:
        print(i, total_earnings)
print("Total Earnings", total_earnings)